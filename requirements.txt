Django==3.0.3
whitenoise==4.1.2
gunicorn
django-cors-headers==3.2.1
requests==2.23.0
pika==1.1.0
django-background-tasks

