from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, FileResponse
from .forms import UploadFileForm
import requests
import random
import string
from .models import ToBeCompressed
import os, glob
from django.conf import settings

# Create your views here.
URL_LOKAL = 'http://localhost:8001/service-2/tugas-3/hanif/'
URL_INFRALABS = 'http://152.118.148.95:20356/service-2/tugas-3/hanif/'
PATH_TO_UPLOADED_FILES_FOLDER = os.path.join(settings.BASE_DIR, 'uploaded-files', 'uploads/')

def index(request):
    cleaningUp()
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            file_name = save_file(request.FILES['file'])
            routing_key = generate_routing_key(file_name, 20)
            send_file_to_compress(PATH_TO_UPLOADED_FILES_FOLDER + file_name, routing_key)
            return render(request, 'core/progress.html', {'routing_key':routing_key})
    else:
        form = UploadFileForm()
    return render(request, 'core/index.html', {'form': form})


def generate_routing_key(file_name, stringLength=10):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

def send_file_to_compress(file_path, routing_key):
    files = {'file': open(file_path,'rb')}
    headers_ = {'X-ROUTING-KEY': routing_key}
    requests.post(URL_INFRALABS, files=files, headers=headers_)

def save_file(file):
    inbody_file = file
    create_object = ToBeCompressed.objects.create(upload=inbody_file)
    file_name = create_object.filename()
    return file_name


def cleaningUp():
    ToBeCompressed.objects.all().delete()
    uploaded_files = glob.glob(PATH_TO_UPLOADED_FILES_FOLDER + '*')

    for uploaded_file in uploaded_files:
        os.remove(uploaded_file)
